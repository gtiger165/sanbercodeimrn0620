//No 1
function range (start, end){
  var number = [];
  if (start > end){
    for (var i = start; i >= end; i--){
      number.push(i);
    }
  } else {
    for (var i = start; i <= end; i++){
      number.push(i);
    }
  }
  
  return number;
}

console.log(range(9, 3));

//No 2
function rangeWithStep(start, end, step){
  var number = [];
  
  if (start > end){
    for (var i = start; i >= end; i-=step){
      number.push(i);
    }
  } else {
    for (var i = start; i <= end; i+=step){
      number.push(i);
    }
  }
  
  return number;
}

console.log(rangeWithStep(11, 1, 2));


//No 3

function sum(start, end, step = 1){
  var result = 0;
  
  if (start > end){
    for (var i = start; i >= end; i-=step){
      result += i;
    }
  } else {
    for (var i = start; i <= end; i+=step){
      result += i;
    }
  }
  
  return result;
}

console.log(sum(20, 10, 2));

//No 4
function dataHandling(input){
  for (var i = 0; i < input.length; i++){
      console.log('Nomor ID : ' + input[i][0]);
      console.log('Nama Lengkap : ' + input[i][1]);
      console.log('TTL : ' + input[i][2] + ' ' + input[i][3]);
      console.log('Hobi : ' + input[i][4]);
      console.log('');
  }
}

var inputData = [
       ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
       ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
       ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
       ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]; 

dataHandling(inputData);

//No 5
function balikKata(kata){
   var reverse = '';
   for(var i = (kata.length - 1); i >= 0; i--){
      reverse += kata[i];
   }
   return reverse;
}

console.log(balikKata("SanberCode"));

//No 6
function dataHandling2(data){
 data.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
 data.splice(4, 1, "Pria", "SMA Internasional Metro");
 console.log(data);
 
 var date = data[3].split("/");
 
 var month = '';
   switch(date[1]) {
	  case '01':   { month = 'Januari'; break; }
	  case '02':   { month = 'Februari'; break; }
	  case '03':   { month = 'Maret'; break; }
	  case '04':   { month = 'April'; break; }
	  case '05':   { month = 'Mei'; break; }
	  case '06':   { month = 'Juni'; break; }
	  case '07':   { month = 'Juli'; break; }
	  case '08':   { month = 'Agustus'; break; }
	  case '09':   { month = 'September'; break; }
	  case '10':   { month = 'Oktober'; break; }
	  case '11':   { month = 'November'; break; }
	  case '12':   { month = 'Desember'; break; }
	  default:  { console.log('Bulan tidak ditemukan');}
 }
 console.log(month);
 
 var reverse = [];
 for(var i = (date.length - 1); i >= 0; i--){
      reverse.push(date[i]);
 }
 console.log(reverse);
 
 var ttl = data[3].split("/");
 var tanggal = ttl.join("-");
 console.log(tanggal);
 
 var nama = data[1].slice(0, 15);
 console.log(nama);
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
