// No 1
function teriak() {
  return 'Halo Sanbers!';
}

console.log(teriak());

// No 2
function kali(angka1, angka2){
 return angka1 * angka2;
}

var num1 = 3;
var num2 = 4;

console.log(kali(num1, num2));

// No3
function perkenalkan(nama, umur, alamat, hobi){
  return 'Nama saya ' + nama + ', umur saya ' + umur + ' tahun, alamat saya di ' + alamat + ', dan saya punya hobby yaitu ' + hobi + '!';
}

var name = 'Drago';
var age = 24;
var address = 'Kalijodo no 12';
var hobby = 'mukbang';

var introduce = perkenalkan(name, age, address, hobby);
console.log(introduce);
