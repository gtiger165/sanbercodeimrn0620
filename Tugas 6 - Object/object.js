//NO 1
function arrayToObject(arr) {
    var now = new Date();
    var thisYear = now.getFullYear();
    var validAge;
    var object = {};
    for(var i=0; i<arr.length; i++){
      object = {
	  firstName: arr[i][0], 
	  lastName: arr[i][1], 
	  gender: arr[i][2], 
	  age: arr[i][3]
      };
     if(object.age == null || object.age > thisYear){
     	object.age = 'Invalid Birth Year';
     } else {
     	object.age = thisYear - object.age;
     }

     var fullname = object.firstName + ' ' + object.lastName;
     console.log((i+1)+". "+fullname+": ");
     console.log(object);
    }
}

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
arrayToObject(people);

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""

//N0 2
function shoppingTime(memberId, money) {
  var sisa
  var member = {}
  var list = []

  if (memberId == null || memberId == '') {
    return 'Mohon maaf, toko X hanya berlaku untuk member saja'
  } else if (money < 50000) {
    return 'Mohon maaf, uang tidak cukup'
  }

  member.memberId = memberId
  member.money = money
  sisa = member.money
  if (sisa >= 1500000) {
    list.push("'Sepatu Stacattu'")
    sisa -= 1500000
  }
  if (sisa >= 500000) {
    list.push("'Baju Zoro'")
    sisa -= 500000
  }

  if (sisa >= 250000) {
    list.push("'Baju H&N")
    sisa -= 250000
  }
  if (sisa >= 175000) {
    list.push("'Sweater Uniklooh'")
    sisa -= 175000
  }
  if (sisa >= 50000) {
    list.push("'Casing Handphone'")
    sisa -= 50000
  }

  member.listPurchased = list
  member.changemoney = sisa

  return member
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//NO 3
function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var jumlah = [];
  var object = {};
  for(var i=0; i<arrPenumpang.length; i++){
     object = {
	  penumpang: arrPenumpang[i][0], 
	  naikDari: arrPenumpang[i][1], 
	  tujuan: arrPenumpang[i][2], 
	  bayar: 0
      };
     
     for(var x=rute.indexOf(object.naikDari); x<rute.length; x++){
	object.bayar += 2000;
     }
     jumlah.push(object);
  }
  return jumlah;
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
