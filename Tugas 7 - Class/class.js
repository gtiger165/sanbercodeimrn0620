//No 1 - release 0
class Animal {
    constructor(nama){
    	this._nama = nama
	this._legs = 4
	this._cold_blooded = false
    }

    get nama(){
    	return this._nama;
    }

    get legs(){
    	return this._legs;
    }

    get cold_blooded() {
    	return this._cold_blooded;
    }
}

var sheep = new Animal("shaun");
 
console.log(sheep.nama) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

//No 1 - release 1
class Ape extends Animal{
  yell(){
     console.log("Auuooo")
  }
}

class Frog extends Animal {
  jump(){
    console.log("Hop hop")
  }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() 
 
var kodok = new Frog("buduk")
kodok.jump() 

//No 2

class Clock {
    constructor({template}){
	this.format = template;
    }

    render(){
 	var date = new Date();

    	var hours = date.getHours();
    	if (hours < 10) hours = '0' + hours;

    	var mins = date.getMinutes();
    	if (mins < 10) mins = '0' + mins;

    	var secs = date.getSeconds();
    	if (secs < 10) secs = '0' + secs;

    	var output = this.format
	.replace('h', hours)
      	.replace('m', mins)
      	.replace('s', secs);

    	console.log(output);
    }

    start() {
	this.render();
    	this.timer = setInterval(this.render.bind(this), 1000);
    }

    stop() {
	clearInterval(this.timer);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 
