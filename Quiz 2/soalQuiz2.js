/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
  // Code disini
  constructor(subject, points, email){
    this.subject = subject;
    this.points = points;
    this.email = email;
  }

  averange() {
    var totalAvg = 0;
    let point = 0;
    if(typeof this.points === 'object' && Array.isArray(this.points)){
      for (let index = 0; index < this.points.length; index++) {
        point += this.points[index];
      }
      totalAvg = point / this.points.length;
    } else {
      totalAvg = this.points;
    }

    console.log('Rata2 Point: ' + totalAvg);
  }
}

/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz-1", "quiz-2", "quiz-3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
  var mSubject = '';
  var outputData = [];

  for (let index = 0; index < data[0].length; index++) {
    if(subject == data[0][index]) mSubject = data[0][index]
  }

  console.log("Subject: "+mSubject)

  for (let index = 1; index < data.length; index++) {
    switch (mSubject) {
      case 'quiz-1':
        var object = {
          email:data[index][0],
          subject:subject,
          points:data[index][1]
        }
        break;
      case 'quiz-2':
        var object = {
          email:data[index][0],
          subject:subject,
          points:data[index][2]
        }
        break;
      case 'quiz-3':
        var object = {
          email:data[index][0],
          subject:subject,
          points:data[index][3]
        }
        break;  
      default:
        break;
    }

    outputData.push(object)
  }

  console.log(outputData)
  return outputData;
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

function recapScores(data) {
  for (let x = 1; x < data.length; x++) {
    var jumlahNilai = 0;
    for (let index = 1; index < data[x].length; index++) {
      jumlahNilai += data[x][index]; 
    }
    var nilaiRata = jumlahNilai/ 3;
    var predikat = '';
    
    if(nilaiRata > 70 && nilaiRata < 80) predikat = 'participant';
    if(nilaiRata > 80 && nilaiRata < 90) predikat = 'graduate';
    if(nilaiRata > 90) predikat = 'honour';

    console.log(x + ". Email: " + data[x][0])
    console.log("Rata -rata: " + nilaiRata)
    console.log("Predikat: " + predikat)
    console.log("")
  }
}

recapScores(data);
