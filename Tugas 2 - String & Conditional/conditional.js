// IF - ELSE
var nama = 'Drago';
var peran = 'guard';

if (nama != null){
 if (peran != null){
  var welcomeMessage = 'Selamat datang di Dunia Werewolf, ' + nama;
  var peranMessage;
  if (peran == 'guard'){
    peranMessage = 'Halo ' + peran + ' ' + nama + ', kamu akan membantu melindungi temanmu dari serangan werewolf';
  } else if (peran == 'werewolf'){
    peranMessage = 'Halo ' + peran + ' ' + nama + ', Kamu akan memakan mangsa setiap malam!';
  } else {
    peranMessage = 'Halo ' + peran + ' ' + nama + ', kamu dapat melihat siapa yang menjadi werewolf!';
  }
  console.log(welcomeMessage);
  console.log(peranMessage);
 } else {
   console.log('Halo ' + nama + ', Pilih peranmu untuk memulai game!');
 }
} else {
 console.log('Nama harus diisi');
}

// SWITCH
var hari = 21; 
var bulan = 6; 
var tahun = 1998;

var day;
var month;
var year;

switch(bulan) {
  case 1:   { month = 'Januari'; break; }
  case 2:   { month = 'Februari'; break; }
  case 3:   { month = 'Maret'; break; }
  case 4:   { month = 'April'; break; }
  case 5:   { month = 'Mei'; break; }
  case 6:   { month = 'Juni'; break; }
  case 7:   { month = 'Juli'; break; }
  case 8:   { month = 'Agustus'; break; }
  case 9:   { month = 'September'; break; }
  case 10:   { month = 'Oktober'; break; }
  case 11:   { month = 'November'; break; }
  case 12:   { month = 'Desember'; break; }
  default:  { console.log('Bulan tidak ditemukan'); }
  }
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 

if (hari > 0 && hari < 32){
 day = hari;
} else {
 console.log('Tanggal tidak ditemukan');
}

if (tahun > 1900 && tahun < 2200){
 year = tahun;	
} else {
 console.log('Tahun Tidak Sesuai');
}

console.log(day+' '+month+' '+year);
